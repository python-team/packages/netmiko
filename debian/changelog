netmiko (4.5.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Colin Watson <cjwatson@debian.org>  Tue, 10 Dec 2024 15:25:29 +0000

netmiko (4.4.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release:
    - Vendor telnetlib library (closes: #1084527).
  * Remove unused setuptools requirement.

 -- Colin Watson <cjwatson@debian.org>  Thu, 24 Oct 2024 21:47:20 +0100

netmiko (4.3.0-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 9 to 13.
  * Set upstream metadata fields: Repository-Browse.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

  [ Andreas Tille ]
  * New upstream version
    Closes: #1035988
  * Real team maintenance in DPT (with permission in
    https://lists.debian.org/debian-python/2024/02/msg00072.html)
  * Standards-Version: 4.6.2 (routine-update)
  * Testsuite: autopkgtest-pkg-python (routine-update)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Repository.
  * watch file standard 4 (routine-update)
  * Fix installation of docs

 -- Andreas Tille <tille@debian.org>  Wed, 28 Feb 2024 11:41:36 +0100

netmiko (2.4.2-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Wed, 04 May 2022 15:17:20 -0400

netmiko (2.4.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/control: Remove trailing whitespaces
  * Use debhelper-compat instead of debian/compat.

  [ Vincent Bernat ]
  * New upstream release.
  * d/control: drop python2 package. Closes: #937130.
  * d/control: add dependency on python3-textfsm.

 -- Vincent Bernat <bernat@debian.org>  Fri, 13 Sep 2019 19:14:59 +0200

netmiko (1.4.3-1) unstable; urgency=medium

  * New upstream release.
  * d/control: bump Standards-Version.

 -- Vincent Bernat <bernat@debian.org>  Sun, 12 Nov 2017 19:46:40 +0100

netmiko (1.1.0-1) unstable; urgency=medium

  * New upstream release.

 -- Vincent Bernat <bernat@debian.org>  Wed, 26 Oct 2016 22:15:09 +0200

netmiko (0.5.6-1) unstable; urgency=medium

  * New upstream release.

 -- Vincent Bernat <bernat@debian.org>  Sat, 03 Sep 2016 12:29:30 +0200

netmiko (0.5.1-1) unstable; urgency=low

  * Initial release (closes: #825417)

 -- Vincent Bernat <bernat@debian.org>  Thu, 26 May 2016 19:04:17 +0000
